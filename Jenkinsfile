def skipRemainingStages = false

pipeline {
  agent { 
    label 'master'
  }

  triggers {
    pollSCM 'H/5 * * * *'
  }

  stages {
    stage('Checkout'){
      steps {
        checkout scm
      }
    }

    stage('Get Commit Message') {
        steps {
            script {
                def script = '''git log -1 --pretty=%%B'''   
                def commitMessage = bat(script: script, returnStdout: true)

                if (!commitMessage.contains("#ci ")){
                  skipRemainingStages = true
                }
            }
        }
    }

    stage ('Restore Packages') {
      when {
        expression {
          !skipRemainingStages
        }
      }

      steps {
        bat "nuget restore"
        bat "dotnet restore"
      }
    }

    stage ('Clean') {
      when {
        expression {
          !skipRemainingStages
        }
      }

      steps {
        bat "dotnet clean"
      }
    }

    stage ('Build') {
      when {
        expression {
          !skipRemainingStages
        }
      }

      steps {
        script {
          def msbuild = tool name: 'MSBuild', type: 'hudson.plugins.msbuild.MsBuildInstallation'
          bat "\"${msbuild}\"MsBuild.exe nGramVisualiser.sln"
        }
      }
    }

    stage ('Archive') {
      when {
        expression {
          !skipRemainingStages
        }
      }

      steps {
        archiveArtifacts artifacts: 'nGramVisualiser/bin/Release/**', allowEmptyArchive: true
      }
    }
  }
}
