import string

from os import listdir
from os.path import isfile, join
from operator import itemgetter
import time

_GramOccurrences = []


def read_file(file_path):
    file = open(file_path, 'r')
    result = file.read()
    return result


def load_from_samples():
    result = ""
    samples_path = "samples"

    files = [f for f in listdir(samples_path) if isfile(join(samples_path, f))]

    for file in files:
        result += read_file(join(samples_path, file))

    return result


def remove_punctuations(text):
    result = ""

    for i in text:
        if i not in string.punctuation:
            result += i
    return result


def add_occurrence_list(gram):
    if gram.strip() == "":
        return

    found = False

    for i in range(len(_GramOccurrences)):
        if list(_GramOccurrences[i])[0] == gram:
            tpl = (gram, list(_GramOccurrences[i])[1] + 1)
            _GramOccurrences.pop(i)
            _GramOccurrences.append(tpl)
            found = True
            break

    if not found:
        tpl = (gram, 1)
        _GramOccurrences.append(tpl)


def custom_compare(e):
    return list(e)[1]


def get_top_n_occurrences(top_count):
    result = []

    if top_count <= 0 or len(_GramOccurrences) == 0:
        return result

    result = sorted(_GramOccurrences, key=itemgetter(1), reverse=True)[:top_count]
    return result


def generate_n_gram(input_data, gram_size):
    result = []

    if gram_size <= 0:
        return result

    input_data = input_data.strip()

    i = 0
    input_data_len = len(input_data)

    while i < input_data_len:
        nth_space_place = i
        first_space = input_data.find(" ", i)

        if first_space >= 0:
            for y in range(gram_size):
                nth_space_place = input_data.find(" ", nth_space_place + y)

            if nth_space_place >= 0:
                gram = input_data[i: nth_space_place]
                gram = remove_punctuations(gram).strip()
                add_occurrence_list(gram)
                i = first_space

        i += 1

    result = get_top_n_occurrences(100)
    return result


if __name__ == '__main__':
    print("Starting...")
    content = load_from_samples()
    print("Data loaded from disk")

    if len(content) <= 0:
        print("Nothing to show...")
        exit(0)

    print("Running 1-Gram")
    before = time.perf_counter()
    grams = generate_n_gram(content, 1)
    after = time.perf_counter()
    print(f"1-Gram Completed in {after - before:0.4f} seconds")
    print(grams)
    _GramOccurrences.clear()
    
    print("Running 2-Gram")
    before = time.perf_counter()
    grams = generate_n_gram(content, 2)
    after = time.perf_counter()
    print(f"2-Gram Completed in {after - before:0.4f} seconds")
    print(grams)
    _GramOccurrences.clear()
    
    print("Running 3-Gram")
    before = time.perf_counter()
    grams = generate_n_gram(content, 3)
    after = time.perf_counter()
    print(f"3-Gram Completed in {after - before:0.4f} seconds")
    print(grams)
    _GramOccurrences.clear()
