﻿namespace nGramVisualizer
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OneGramTB = new System.Windows.Forms.RichTextBox();
            this.TwoGramsTB = new System.Windows.Forms.RichTextBox();
            this.ThreeGramsTB = new System.Windows.Forms.RichTextBox();
            this.filesLB = new System.Windows.Forms.ListBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.AnalyseBtn = new System.Windows.Forms.Button();
            this.RemoveFileBtn = new System.Windows.Forms.Button();
            this.AddFileBtn = new System.Windows.Forms.Button();
            this.RemoveAllBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // OneGramTB
            // 
            this.OneGramTB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.OneGramTB.Location = new System.Drawing.Point(12, 153);
            this.OneGramTB.Name = "OneGramTB";
            this.OneGramTB.ReadOnly = true;
            this.OneGramTB.Size = new System.Drawing.Size(298, 345);
            this.OneGramTB.TabIndex = 2;
            this.OneGramTB.Text = "";
            // 
            // TwoGramsTB
            // 
            this.TwoGramsTB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TwoGramsTB.Location = new System.Drawing.Point(316, 153);
            this.TwoGramsTB.Name = "TwoGramsTB";
            this.TwoGramsTB.ReadOnly = true;
            this.TwoGramsTB.Size = new System.Drawing.Size(404, 345);
            this.TwoGramsTB.TabIndex = 3;
            this.TwoGramsTB.Text = "";
            // 
            // ThreeGramsTB
            // 
            this.ThreeGramsTB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ThreeGramsTB.Location = new System.Drawing.Point(726, 153);
            this.ThreeGramsTB.Name = "ThreeGramsTB";
            this.ThreeGramsTB.ReadOnly = true;
            this.ThreeGramsTB.Size = new System.Drawing.Size(478, 345);
            this.ThreeGramsTB.TabIndex = 4;
            this.ThreeGramsTB.Text = "";
            // 
            // filesLB
            // 
            this.filesLB.FormattingEnabled = true;
            this.filesLB.Location = new System.Drawing.Point(12, 13);
            this.filesLB.Name = "filesLB";
            this.filesLB.Size = new System.Drawing.Size(1068, 134);
            this.filesLB.TabIndex = 8;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // AnalyseBtn
            // 
            this.AnalyseBtn.Location = new System.Drawing.Point(1086, 124);
            this.AnalyseBtn.Name = "AnalyseBtn";
            this.AnalyseBtn.Size = new System.Drawing.Size(118, 23);
            this.AnalyseBtn.TabIndex = 12;
            this.AnalyseBtn.Text = "Analyse";
            this.AnalyseBtn.UseVisualStyleBackColor = true;
            this.AnalyseBtn.Click += new System.EventHandler(this.AnalyseBtn_Click);
            // 
            // RemoveFileBtn
            // 
            this.RemoveFileBtn.Location = new System.Drawing.Point(1086, 42);
            this.RemoveFileBtn.Name = "RemoveFileBtn";
            this.RemoveFileBtn.Size = new System.Drawing.Size(118, 23);
            this.RemoveFileBtn.TabIndex = 13;
            this.RemoveFileBtn.Text = "Remove Selected";
            this.RemoveFileBtn.UseVisualStyleBackColor = true;
            this.RemoveFileBtn.Click += new System.EventHandler(this.RemoveFileBtn_Click);
            // 
            // AddFileBtn
            // 
            this.AddFileBtn.Location = new System.Drawing.Point(1086, 13);
            this.AddFileBtn.Name = "AddFileBtn";
            this.AddFileBtn.Size = new System.Drawing.Size(118, 23);
            this.AddFileBtn.TabIndex = 14;
            this.AddFileBtn.Text = "Add File...";
            this.AddFileBtn.UseVisualStyleBackColor = true;
            this.AddFileBtn.Click += new System.EventHandler(this.AddFileBtn_Click);
            // 
            // RemoveAllBtn
            // 
            this.RemoveAllBtn.Location = new System.Drawing.Point(1086, 71);
            this.RemoveAllBtn.Name = "RemoveAllBtn";
            this.RemoveAllBtn.Size = new System.Drawing.Size(118, 23);
            this.RemoveAllBtn.TabIndex = 15;
            this.RemoveAllBtn.Text = "Remove All";
            this.RemoveAllBtn.UseVisualStyleBackColor = true;
            this.RemoveAllBtn.Click += new System.EventHandler(this.RemoveAllBtn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1216, 510);
            this.Controls.Add(this.RemoveAllBtn);
            this.Controls.Add(this.AddFileBtn);
            this.Controls.Add(this.RemoveFileBtn);
            this.Controls.Add(this.AnalyseBtn);
            this.Controls.Add(this.filesLB);
            this.Controls.Add(this.ThreeGramsTB);
            this.Controls.Add(this.TwoGramsTB);
            this.Controls.Add(this.OneGramTB);
            this.Name = "Form1";
            this.Text = "nGram Visualiser";
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.RichTextBox OneGramTB;
        private System.Windows.Forms.RichTextBox TwoGramsTB;
        private System.Windows.Forms.RichTextBox ThreeGramsTB;
        private System.Windows.Forms.ListBox filesLB;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button AnalyseBtn;
        private System.Windows.Forms.Button RemoveFileBtn;
        private System.Windows.Forms.Button AddFileBtn;
        private System.Windows.Forms.Button RemoveAllBtn;
    }
}

