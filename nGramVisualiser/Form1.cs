﻿using nGramVisualiser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nGramVisualizer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            SetFilesListBox();
            SetOpenFileDialog();
        }


        private delegate void DelWriteToTextBox(RichTextBox tb, string text);
        private void WriteToTextBox(RichTextBox tb, string text)
        {
            if (InvokeRequired)
                Invoke(new DelWriteToTextBox(WriteToTextBox), new object[2] { tb, text });
            else
            {
                tb.Text = text;
            }
        }

        private delegate void DelWriteToButton(Button btn, string text);
        private void WriteToButton(Button btn, string text)
        {
            if (InvokeRequired)
                Invoke(new DelWriteToButton(WriteToButton), new object[2] { btn, text });
            else
            {
                btn.Text = text;
            }
        }

        private void ClearOldData()
        {
            _InputData = string.Empty;
            WriteToTextBox(OneGramTB, string.Empty);
            WriteToTextBox(TwoGramsTB, string.Empty);
            WriteToTextBox(ThreeGramsTB, string.Empty);
        }

        private static string _InputData = string.Empty;

        private void CollectInputData()
        {
            foreach (var item in filesLB.Items)
            {
                var fi = new System.IO.FileInfo(((Tuple<string, string>)item).Item2);

                if (fi.Exists)
                {
                    var fContent = System.IO.File.ReadAllText(fi.FullName);
                    _InputData += $"{fContent}{Environment.NewLine}";
                }

            }
        }


        private async Task RunNGramAsync(RichTextBox tb, int gramCount,
    CancellationToken cancellationToken)
        {
            await Task.Run(() =>
            {
                try
                {
                    var gramText = string.Empty;
                    var gramStopWatch = new System.Diagnostics.Stopwatch();
                    gramStopWatch.Start();

                    var gramHelper = new NGramHelper();
                    var result = gramHelper.GenerateNGram(_InputData, gramCount, cancellationToken);

                    if (cancellationToken.IsCancellationRequested)
                    {
                        gramText = "Cancelled!";
                    }
                    else
                    {
                        foreach (var item in result)
                        {
                            gramText += $"{item.Item1} ({item.Item2}){Environment.NewLine}";
                        }
                    }
                    gramStopWatch.Stop();

                    WriteToTextBox(tb, $"Completed in {FormatTimeSpan(gramStopWatch.Elapsed)}{Environment.NewLine}{gramText}");
                }
                catch (Exception ex)
                {
                }
            });
        }

        private void AnalyseNGrams()
        {
            if (string.IsNullOrWhiteSpace(_InputData)) return;

            var task1 = RunNGramAsync(OneGramTB, 1, _cts.Token);
            var task2 = RunNGramAsync(TwoGramsTB, 2, _cts.Token);
            var task3 = RunNGramAsync(ThreeGramsTB, 3, _cts.Token);

            Task.WaitAll(task1, task2, task3);
            _InputData = string.Empty;

            GC.Collect();
        }


        private static CancellationTokenSource _cts = new CancellationTokenSource();
        private bool _isAnalysing = false;
        private bool _IsAnalysing
        {
            get { return _isAnalysing; }
            set
            {
                if (value != _isAnalysing)
                {
                    _isAnalysing = value;

                    if (value)
                        WriteToButton(AnalyseBtn, "Cancel");
                    else
                        WriteToButton(AnalyseBtn, "Analyse");
                }
            }
        }


        private void AnalyseBtn_Click(object sender, EventArgs e)
        {
            if (sender is Button btn)
            {
                if (_IsAnalysing)
                {
                    _cts.Cancel();
                }
                else
                {
                    _IsAnalysing = true;

                    ClearOldData();
                    CollectInputData();

                    _cts = new CancellationTokenSource();
                    Task.Run(() =>
                    {
                        AnalyseNGrams();
                        _IsAnalysing = false;
                    });
                }
            }
        }

        private void AddFileBtn_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
        }

        private void SetOpenFileDialog()
        {
            openFileDialog1.Filter = "Text files (*.txt)|*.txt";
            openFileDialog1.FileName = "Samples";
            openFileDialog1.Multiselect = true;

            openFileDialog1.FileOk += OpenFileDialog1_FileOk;
        }

        private void SetFilesListBox()
        {
            filesLB.SelectionMode = SelectionMode.MultiExtended;
        }

        private string FormatTimeSpan(TimeSpan ts)
        {
            var result = string.Empty;

            if (ts.Hours > 0)
            {
                result += $"{ts.Hours} hour(s) ";
            }

            if (ts.Minutes > 0)
            {
                result += $"{ts.Minutes} minute(s) ";
            }

            if (ts.Seconds > 0)
            {
                result += $"{ts.Seconds} second(s) ";
            }

            if (ts.Milliseconds > 0)
            {
                result += $"{ts.Milliseconds} milisecond(s) ";
            }

            return result;
        }
        private void OpenFileDialog1_FileOk(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (sender is System.Windows.Forms.FileDialog fd)
            {
                foreach (var item in fd.FileNames)
                {
                    var fi = new System.IO.FileInfo(item);

                    filesLB.Items.Add(new Tuple<string, string>(fi.Name, fi.FullName));
                }
            }
        }

        private void RemoveFileBtn_Click(object sender, EventArgs e)
        {
            var itemsToRemove = new List<object>();
            foreach (var item in filesLB?.SelectedItems)
            {
                itemsToRemove.Add(item);
            }

            foreach (var item in itemsToRemove)
            {
                filesLB.Items.Remove(item);
            }
        }

        private void RemoveAllBtn_Click(object sender, EventArgs e)
        {
            filesLB.Items.Clear();
        }
    }
}
