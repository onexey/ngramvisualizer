﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace nGramVisualiser
{
    public class NGramHelper
    {
        public NGramHelper()
        {

        }

        private List<Tuple<string, int>> _GramOccurrences = new List<Tuple<string, int>>();

        public List<Tuple<string, int>> GenerateNGram(string inputData, int gramSize, CancellationToken cancellationToken)
        {
            var result = new List<Tuple<string, int>>();
            if (gramSize <= 0) return result;
            try
            {

                inputData = inputData.Trim();
                for (int i = 0; i < inputData.Length; i++)
                {
                    if (cancellationToken.IsCancellationRequested) break;

                    var nthSpacePlace = i;
                    var firstSpace = inputData.IndexOf(' ', i);

                    for (int y = 0; y < gramSize; y++)
                    {
                        nthSpacePlace = inputData.IndexOf(' ', nthSpacePlace + y);
                    }

                    if (nthSpacePlace >= 0)
                    {
                        var gram = inputData.Substring(i, nthSpacePlace - i);
                        gram = RemovePunctuations(gram).Trim();
                        AddOccurrenceList(gram);
                        i = firstSpace;
                    }
                }
            }
            catch (Exception ex)
            {
                
            }

            result = GetTopNOccurrences(100);

            return result;
        }

        private List<Tuple<string, int>> GetTopNOccurrences(int topCount)
        {
            var result = new List<Tuple<string, int>>();

            if (topCount <= 0) return result;
            result = _GramOccurrences.OrderByDescending(a => a.Item2).Take(topCount).ToList();

            return result;
        }

        private void AddOccurrenceList(string gram)
        {
            if (string.IsNullOrWhiteSpace(gram)) return;
            
            var item = _GramOccurrences.Where(a => a.Item1 == gram).DefaultIfEmpty(null).FirstOrDefault();

            if (item == null)
            {
                var tuple = new Tuple<string, int>(gram, 1);
                _GramOccurrences.Add(tuple);
            }
            else
            {
                var tuple = new Tuple<string, int>(gram, item.Item2 + 1);
                _GramOccurrences.Remove(item);
                _GramOccurrences.Add(tuple);
            }
        }


        private string RemovePunctuations(string text)
        {
            var result = string.Empty;

            for (int i = 0; i < text.Length; i++)
            {
                if (!char.IsPunctuation(text[i]))
                {
                    result += text[i];
                }
            }

            return result;
        }
    }
}
